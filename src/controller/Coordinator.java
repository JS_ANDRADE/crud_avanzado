/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.help.HelpSetException;

import model.Logic;
import view.PanelProduct;
import view.PanelVehicle;
import view.viewInsertProduct;

import view.viewSearchProducts;
import view.WindowMain;
import view.viewInsertVehicle;
import view.viewModifyProduct;
import view.viewModifyVehicle;
import view.viewSearchVehicles;

/**
 *
 * Clase que coordina todos lso componentes del programa
 * 
 * @author JStalin
 */
public class Coordinator {
    
    private ControlProduct controlProduct;
    private ControlVehicle controlVehicle;
    
    private Logic logic;
    
    private WindowMain windowMain;
    
    private viewSearchProducts searchProduct;
    private viewInsertProduct insertProduct;
    private viewModifyProduct modifyProduct;
    
    
   private viewSearchVehicles searchVehicle;
    private viewInsertVehicle insertVehicle;
    private viewModifyVehicle modifyVehicle;

    
    
    /**
     * Constrcutro por defecto
     */
    public Coordinator() {
    }

    
    public ControlProduct getControlProduct() {
        return controlProduct;
    }

    public void setControlProduct(ControlProduct controlProduct) {
        this.controlProduct = controlProduct;
    }

    public ControlVehicle getControlVehicle() {
        return controlVehicle;
    }

    public void setControlVehicle(ControlVehicle controlVehicle) {
        this.controlVehicle = controlVehicle;
    }

    public Logic getLogic() {
        return logic;
    }

    public void setLogic(Logic logic) {
        this.logic = logic;
    }

    public WindowMain getWindowMain() {
        return windowMain;
    }

    public void setWindowMain(WindowMain windowMain) {
        this.windowMain = windowMain;
    }

    public viewSearchProducts getSearchProduct() {
        return searchProduct;
    }

    public void setSearchProduct(viewSearchProducts searchProduct) {
        this.searchProduct = searchProduct;
    }

   

    public viewInsertProduct getInserProduct() {
        return insertProduct;
    }

    public void setInsertProduct(viewInsertProduct insertProduct) {
        this.insertProduct = insertProduct;
    }

    public void setModifyProduct(viewModifyProduct modifyProduct) {
        this.modifyProduct = modifyProduct;
    }

    public viewSearchVehicles getSearchVehicle() {
        return searchVehicle;
    }

    public void setSearchVehicle(viewSearchVehicles searchVehicle) {
        this.searchVehicle = searchVehicle;
    }

    public viewInsertVehicle getInsertVehicle() {
        return insertVehicle;
    }

    public void setInsertVehicle(viewInsertVehicle insertVehicle) {
        this.insertVehicle = insertVehicle;
    }

    public viewModifyVehicle getModifyVehicle() {
        return modifyVehicle;
    }

    public void setModifyVehicle(viewModifyVehicle modifyVehicle) {
        this.modifyVehicle = modifyVehicle;
    }

    public viewInsertProduct getInsertProduct() {
        return insertProduct;
    }

    public viewModifyProduct getModifyProduct() {
        return modifyProduct;
    }

   

    public void addHelpComponentes(){
        
        
        try {

          
            
            URL hsURL = getClass().getResource("/help/help_set.hs");
            
            HelpSet helpset = new HelpSet(getClass().getClassLoader(), hsURL);

            HelpBroker hb = helpset.createHelpBroker();
            
            hb.enableHelpKey(this.getWindowMain().getContentPane(), "main", helpset);
            
            
            
           
            

        } catch (HelpSetException ex) {
            Logger.getLogger(PanelVehicle.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
    }
    
    
    
    
}

package controller;

import model.vo.VehicleVO;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import model.Logic;
import table.Table_View_Vehicle;
import view.viewInsertVehicle;
import view.viewModifyVehicle;
import view.viewSearchVehicles;

/**
 * Clase para controlar las acciones sobre los vehiculos
 * @author JStalin
 */
public class ControlVehicle {

    EventsWindow eWindow = new EventsWindow();
    EventsMouse eMouse = new EventsMouse();
    EventsFocus eFocus = new EventsFocus();
    EventsKey eKey = new EventsKey();

    private Coordinator coordinator;

    private Logic logic;

    private viewSearchVehicles searchVehicles;
    private viewInsertVehicle insertVehicles;
    private viewModifyVehicle modifyVehicle;

    /**
     * COnstructor por defecto
     */
    public ControlVehicle() {

    }

    /**
     * Metodo para iniciar los componentes de la clase
     */
    private void initComponents() {

        this.logic = coordinator.getLogic();

    }

    /**
     * Metodo para iniciar una nueva ventana de busqueda de vehiculos
     */
    public void initViewSearchVehicle() {

        newViewSearch();

    }

    /**
     * MEtodo para crear y mostrar ventan de vusqueda de vehiculos
     */
    private void newViewSearch() {

        searchVehicles = new viewSearchVehicles(coordinator.getWindowMain(), true);

        
        
        coordinator.setSearchVehicle(searchVehicles);

        searchVehicles.addWindowListener(eWindow);
        searchVehicles.txt_search.addKeyListener(eKey);
        searchVehicles.btn_all.addMouseListener(eMouse);
        searchVehicles.btn_search.addMouseListener(eMouse);
        searchVehicles.txt_search.addFocusListener(eFocus);

        searchVehicles.tableVehicles.addMouseListener(eMouse);
        
        searchVehicles.setHelp();

        searchVehicles.setVisible(true);

    }

    /**
     * Metodo para inciiar una nueva venta para insertar vehiculos
     */
    public void initViewInsertVehicle() {

        newViewInsert();

    }

    /**
     * Metodo que crear y muestra una ventana de insertar vehiculos
     */
    private void newViewInsert() {

        insertVehicles = new viewInsertVehicle(coordinator.getWindowMain(), true);

        coordinator.setInsertVehicle(insertVehicles);

        insertVehicles.btn_insert.addMouseListener(eMouse);

        insertVehicles.setVisible(true);

    }

    /**
     * Metodo para iniciarlizar unna nuevan ventana para modificar vehiculos
     * @param data --  Datos con el vehiculo a modificar
     */
    public void initViewModifyVehicle(VehicleVO data) {
        newViewModify(data);
    }

    /**
     * Metodo que crea y muestra ventan de modificar vehiculos
     * @param data 
     */
    private void newViewModify(VehicleVO data) {

        modifyVehicle = new viewModifyVehicle(coordinator.getWindowMain(), true);

        coordinator.setModifyVehicle(modifyVehicle);

        modifyVehicle.btn_update.addMouseListener(eMouse);
        modifyVehicle.txt_enrollment.addFocusListener(eFocus);

        if (data != null) {

            updatePropertyesModify(data);
            updateFields(data);
        } else {
            setInvisibleOldPropertyes();
        }

        modifyVehicle.setVisible(true);
    }

    /**
     * Metodo para esconder los componentes del panel de antiguas propeidades
     */
    private void setInvisibleOldPropertyes() {

        modifyVehicle.lbl_oldPropertyes.setVisible(false);
        modifyVehicle.jLabel1.setVisible(false);
        modifyVehicle.jLabel2.setVisible(false);
        modifyVehicle.jLabel3.setVisible(false);
        modifyVehicle.jLabel4.setVisible(false);
        modifyVehicle.jLabel5.setVisible(false);
        modifyVehicle.lbl_oldEnrollment.setVisible(false);
        modifyVehicle.lbl_oldBrand.setVisible(false);
        modifyVehicle.lbl_oldModel.setVisible(false);
        modifyVehicle.lbl_oldKilometers.setVisible(false);
        modifyVehicle.lbl_oldDateReview.setVisible(false);

    }

    /**
     * Metodo para visualizar los componentes del panel antiguas propiedades
     */
    private void setVisibleOldPropertyes() {
        modifyVehicle.lbl_oldPropertyes.setVisible(true);
        modifyVehicle.jLabel1.setVisible(true);
        modifyVehicle.jLabel2.setVisible(true);
        modifyVehicle.jLabel3.setVisible(true);
        modifyVehicle.jLabel4.setVisible(true);
        modifyVehicle.jLabel5.setVisible(true);
        modifyVehicle.lbl_oldEnrollment.setVisible(true);
        modifyVehicle.lbl_oldBrand.setVisible(true);
        modifyVehicle.lbl_oldModel.setVisible(true);
        modifyVehicle.lbl_oldKilometers.setVisible(true);
        modifyVehicle.lbl_oldDateReview.setVisible(true);
    }

    /**
     * Metodo para actualizar las propiedades antiguas del panel
     * @param data -- Datos a actualizar
     */
    public void updatePropertyesModify(VehicleVO data) {

        setVisibleOldPropertyes();

        modifyVehicle.lbl_oldEnrollment.setText(data.getEnrollment());
        modifyVehicle.lbl_oldBrand.setText(data.getBrand());
        modifyVehicle.lbl_oldModel.setText(data.getModel());
        modifyVehicle.lbl_oldKilometers.setText(data.getKilometers() + " Km");
        
        
        SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy");
        modifyVehicle.lbl_oldDateReview.setText(formatDate.format(data.getDateRevision()));

    }

     /**
     * Metodo para actualizar los campos del panel modificar vehiculos
     * @param data -- Datos para actualizar
     */
    private void updateFields(VehicleVO data) {

        modifyVehicle.txt_enrollment.setText(data.getEnrollment());
        modifyVehicle.txt_enrollment.setEnabled(false);

        modifyVehicle.txt_brand.setText(data.getBrand());
        modifyVehicle.txt_model.setText(data.getModel());
        modifyVehicle.txt_kilometers.setText(data.getKilometers() + "");
        
        SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy");
        
        modifyVehicle.txt_dateReview.setText(formatDate.format(data.getDateRevision()));

    }

    /**
     * Metodo que se ejcutara al pulsar el boton de insertar vehiculo
     */
    private void insertVehicle() {

        VehicleVO producInserted = obtainVehicleDataInsert();

        logic.validateRegistryVehicle(producInserted);

    }

    /**
     * Metodo que se ejecutar al pulsar boton de actualizar vehiculo
     */
    private void updateVehicle() {
        VehicleVO producUpdate = obtainVehicleDataModify();

        logic.validateModifyVehicle(producUpdate);
    }

     /**
     * Metodo que obtiene la informacion de los campos del dialogo de insertar
     * vehiculo
     * @return  Objeto  VehicleVO con los datos
     */
    private VehicleVO obtainVehicleDataInsert() {

        VehicleVO vehicle = new VehicleVO();

        vehicle.setEnrollment(insertVehicles.txt_enrollment.getText());
        vehicle.setBrand(insertVehicles.txt_brand.getText());
        vehicle.setModel(insertVehicles.txt_model.getText());

        try {
            vehicle.setKilometers(Integer.parseInt(insertVehicles.txt_kilometers.getText()));

        } catch (NumberFormatException e) {
            
        }

        SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy");

        try {
            vehicle.setDateRevision(new Date(formatDate.parse(insertVehicles.txt_dateReview.getText()).getTime()));
        } catch (ParseException ex) {
            
        }

        return vehicle;
    }

     /**
     * Metodo para obtener la informacion de los campos del dialogo modificar
     * vehiculo
     * @return  Objeto VehicleVO con los datos
     */
    private VehicleVO obtainVehicleDataModify() {

        VehicleVO vehicle = new VehicleVO();

        vehicle.setEnrollment(modifyVehicle.txt_enrollment.getText());
        vehicle.setBrand(modifyVehicle.txt_brand.getText());
        vehicle.setModel(modifyVehicle.txt_model.getText());

        try {
            vehicle.setKilometers(Integer.parseInt(modifyVehicle.txt_kilometers.getText()));

        } catch (NumberFormatException e) {
           
        }

        SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy");

        try {
            vehicle.setDateRevision(new Date(formatDate.parse(modifyVehicle.txt_dateReview.getText()).getTime()));
        } catch (ParseException ex) {
           
        }

        return vehicle;
    }

    /**
     * Metodo para declarar un coordinador a la clase
     * @param c 
     */
    public void setCoordinator(Coordinator c) {

        this.coordinator = c;
        initComponents();

    }

     /**
     * Metodo para poner en la tabla todos los vehiculos
     */
    public void setAllVehicles() {

        Table_View_Vehicle tableView = new Table_View_Vehicle();

        JTable table = searchVehicles.tableVehicles;
        Object[] header = logic.getColumnsVehicle();
        ArrayList<VehicleVO> data = logic.validateQueryVehicles();

        if (data.size() <= 0) {

            searchVehicles.pnlTable.setVisible(false);
            searchVehicles.noResults.setVisible(true);

        } else {

            searchVehicles.pnlTable.setVisible(true);
            searchVehicles.noResults.setVisible(false);

            tableView.setDataInTable(table, data, header);
            coordinator.getSearchVehicle().tableVehicles.updateUI();
        }

    }

     /**
     * Metodo que se ejcuta al perder el focu del campo matricula
     */
    private void actionFocusLostEnrollment() {

        String enrollment = modifyVehicle.txt_enrollment.getText();

        ArrayList<VehicleVO> vehicle = logic.validateQueryVehiclesBySearch(enrollment);

        if (vehicle.size() == 1) {
            modifyVehicle.lbl_errorEnrollment.setVisible(false);
            updatePropertyesModify(vehicle.get(0));
            updateFields(vehicle.get(0));
            modifyVehicle.txt_enrollment.setEnabled(true);

        } else {

            setInvisibleOldPropertyes();
            modifyVehicle.reset();

            modifyVehicle.lbl_errorEnrollment.setText("No vehicle");
            modifyVehicle.lbl_errorEnrollment.setVisible(true);

        }

    }

    /**
     * Metodo que pone en la tabla los datos de los vehiculos en relacion
     *  a la busqueda
     */
    public void setSearchVehicles() {

        String search = getStringSearch();

        Table_View_Vehicle tableView = new Table_View_Vehicle();

        JTable table = searchVehicles.tableVehicles;
        Object[] header = logic.getColumnsVehicle();
        ArrayList<VehicleVO> data = logic.validateQueryVehiclesBySearch(search);
        System.out.println("DAta search" + data.size());

        if (data.size() <= 0) {

            searchVehicles.pnlTable.setVisible(false);

            searchVehicles.noResults.setVisible(true);

        } else {

            searchVehicles.pnlTable.setVisible(true);

            searchVehicles.noResults.setVisible(false);

            tableView.setDataInTable(table, data, header);
            coordinator.getSearchVehicle().tableVehicles.updateUI();
        }

    }

     /**
     * MEtodo que obtiene los datos de la busqueda
     * @return  Cadena con la busqueda
     */
    private String getStringSearch() {

        return searchVehicles.txt_search.getText();


    }
    /**
     * Metodo que se ejecuta al pulsar en el boton de Busqueda
     */
    private void pressedSearchVehicle() {

        searchVehicles.setLblColor(searchVehicles.btn_search);
        searchVehicles.resetLblColor(searchVehicles.btn_all);

    }

    /**
     * Metodo que se ejecuta al pulsar en el boton de eliminar
     * @param enrollmentVehicle  --> Referencia del vehiculo que se va a eliminar
     */
    private void actionButtonDelete(String enrollmentVehicle) {

        VehicleVO vehicleDelete = new VehicleVO(enrollmentVehicle);

        logic.validateDeleteVehicle(vehicleDelete);

    }

    /**
     * Metodo que gestiona los eventos de la tabla
     * @param evt Evento que gestiona
     */
    private void eventTable(MouseEvent evt) {
        int clic_tabla = searchVehicles.tableVehicles.rowAtPoint(evt.getPoint());

        int column = searchVehicles.tableVehicles.getColumnModel().getColumnIndexAtX(evt.getX());
        int row = evt.getY() / searchVehicles.tableVehicles.getRowHeight();

        if (row < searchVehicles.tableVehicles.getRowCount() && row >= 0 && column < searchVehicles.tableVehicles.getColumnCount() && column >= 0) {
            Object value = searchVehicles.tableVehicles.getValueAt(row, column);
            if (value instanceof JButton) {
                ((JButton) value).doClick();
                JButton boton = (JButton) value;

                if (boton.getName().equals("m")) {

                    int rowSelected = searchVehicles.tableVehicles.getSelectedRow();

                    VehicleVO vehicleSelected = new VehicleVO();

                    vehicleSelected.setEnrollment((String) searchVehicles.tableVehicles.getValueAt(rowSelected, 0));
                    vehicleSelected.setBrand((String) searchVehicles.tableVehicles.getValueAt(rowSelected, 1));
                    vehicleSelected.setModel((String) searchVehicles.tableVehicles.getValueAt(rowSelected, 2));
                    vehicleSelected.setKilometers((int) searchVehicles.tableVehicles.getValueAt(rowSelected, 3));
                    vehicleSelected.setDateRevision((Date) searchVehicles.tableVehicles.getValueAt(rowSelected, 4));

                    initViewModifyVehicle(vehicleSelected);

                }
                if (boton.getName().equals("e")) {

                    int rowSelected = searchVehicles.tableVehicles.getSelectedRow();

                    String enrollmentSelected = (String) searchVehicles.tableVehicles.getValueAt(rowSelected, 0);

                    actionButtonDelete(enrollmentSelected);
                }
            }

        }
    }

    /**
     * Clase interna para gestionar eventos de ventana
     */
    class EventsWindow extends WindowAdapter {

        @Override
        public void windowOpened(WindowEvent e) {

            if (e.getWindow() == searchVehicles) {
                setAllVehicles();
            }

        }

    }

     /**
     * Clase Interna para gestiona eventos del raton
     */
    class EventsMouse extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {

            if (searchVehicles != null && searchVehicles.isActive()) {

                if (e.getSource() == searchVehicles.btn_all) {

                    coordinator.getSearchVehicle().pulsedButton = 0;
                    setAllVehicles();
                }

                if (e.getSource() == searchVehicles.btn_search) {

                    coordinator.getSearchVehicle().pulsedButton = 1;
                    setSearchVehicles();
                }

                if (e.getSource() == searchVehicles.tableVehicles) {

                    eventTable(e);

                }
            }

            if (insertVehicles != null && insertVehicles.isActive()) {
                if (e.getSource() == insertVehicles.btn_insert) {

                    insertVehicle();

                }
            }

            if (modifyVehicle != null && modifyVehicle.isActive()) {
                if (e.getSource() == modifyVehicle.btn_update) {

                    updateVehicle();
                    if (!modifyVehicle.txt_enrollment.isEditable()) {
                        modifyVehicle.dispose();
                    }

                }

            }

        }

    }

     /**
     * Clase interna para gestionar eventos de foco
     */
    class EventsFocus implements FocusListener {

        @Override
        public void focusGained(FocusEvent e) {

            if (searchVehicles != null) {
                if (e.getSource() == searchVehicles.txt_search) {

                    if (searchVehicles.txt_search.getText().equals("Search...")) {

                        searchVehicles.txt_search.setText("");

                    }

                }
            }

        }

        @Override
        public void focusLost(FocusEvent e) {

            if (searchVehicles != null) {
                if (e.getSource() == searchVehicles.txt_search) {

                    if (searchVehicles.txt_search.getText().equals("")) {

                        searchVehicles.txt_search.setText("Search...");

                    }

                }
            }

            if (modifyVehicle != null) {

                if (e.getSource() == modifyVehicle.txt_enrollment) {

                    actionFocusLostEnrollment();

                }

            }
        }

    }

     /**
     * Clase interna para gestionar eventos de teclado
     */
    class EventsKey extends KeyAdapter {

        @Override
        public void keyPressed(KeyEvent e) {

            int codeKeyPressed = e.getKeyCode();

            if (codeKeyPressed == KeyEvent.VK_ENTER) {
                pressedSearchVehicle();
                setSearchVehicles();

                coordinator.getSearchVehicle().pulsedButton = 1;

            }

        }

    }

}

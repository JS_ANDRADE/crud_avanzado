package controller;

import model.vo.ProductVO;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;


import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import model.Logic;
import table.Table_View_Product;
import view.WindowMain;
import view.viewInsertProduct;
import view.viewModifyProduct;
import view.viewSearchProducts;

/**
 * Clase que permite controlar las operaciones sobre los productos
 *
 * @author JStalin
 */
public class ControlProduct {

    EventsWindow eWindow = new EventsWindow();
    EventsMouse eMouse = new EventsMouse();
    EventsFocus eFocus = new EventsFocus();
    EventsKey eKey = new EventsKey();

    private Coordinator coordinator;

    private Logic logic;

    private viewSearchProducts searchProducts;
    private viewInsertProduct insertProducts;
    private viewModifyProduct modifyProduct;

    /**
     * Constructor por defecto
     */
    public ControlProduct() {

    }

    /**
     * Metodo para iniciar los componntes de la clase
     */
    private void initComponents() {

        this.logic = coordinator.getLogic();

    }

    /**
     * Metodo para iniciar una nueva venta de busqueda de productos
     */
    public void initViewSearchProduct() {

        newViewSearch();

    }

    /**
     * Metodo que crea una ventan de busqueda de productos y la muestra
     */
    private void newViewSearch() {

        searchProducts = new viewSearchProducts(coordinator.getWindowMain(), true);

        coordinator.setSearchProduct(searchProducts);

        searchProducts.addWindowListener(eWindow);
        searchProducts.txt_search.addKeyListener(eKey);
        searchProducts.btn_all.addMouseListener(eMouse);
        searchProducts.btn_search.addMouseListener(eMouse);
        searchProducts.txt_search.addFocusListener(eFocus);

        searchProducts.tableProducts.addMouseListener(eMouse);

        searchProducts.setHelp();

        searchProducts.setVisible(true);

    }

    /**
     * Metodo para iniciar una nueva ventan de insertar productos
     */
    public void initViewInsertProduct() {

        newViewInsert();

    }

    /**
     * Metodo para crear y muestra una ventan de insertar producto
     */
    private void newViewInsert() {

        insertProducts = new viewInsertProduct(coordinator.getWindowMain(), true);

        coordinator.setInsertProduct(insertProducts);

        insertProducts.btn_insert.addMouseListener(eMouse);

        insertProducts.setVisible(true);

    }

    /**
     * Metodo para iniciar una ventan de modificiacion de productos
     *
     * @param data
     */
    public void initViewModifyProduct(ProductVO data) {
        newViewModify(data);
    }

    /**
     * Metodo para crear y mostrar una ventan de modificiacion de productos
     *
     * @param data
     */
    private void newViewModify(ProductVO data) {

        modifyProduct = new viewModifyProduct(coordinator.getWindowMain(), true);

        coordinator.setModifyProduct(modifyProduct);

        modifyProduct.btn_update.addMouseListener(eMouse);
        modifyProduct.txt_reference.addFocusListener(eFocus);

        if (data != null) {

            updatePropertyesModify(data);
            updateFields(data);
        } else {
            setInvisibleOldPropertyes();
        }

        modifyProduct.setVisible(true);
    }

    /**
     * Metodo para esconder los componentes del panel de antiguas propeidades
     */
    private void setInvisibleOldPropertyes() {

        modifyProduct.lbl_oldPropertyes.setVisible(false);
        modifyProduct.jLabel1.setVisible(false);
        modifyProduct.jLabel2.setVisible(false);
        modifyProduct.jLabel3.setVisible(false);
        modifyProduct.jLabel4.setVisible(false);
        modifyProduct.jLabel5.setVisible(false);
        modifyProduct.lbl_oldReference.setVisible(false);
        modifyProduct.lbl_oldName.setVisible(false);
        modifyProduct.lbl_oldDescription.setVisible(false);
        modifyProduct.lbl_oldPrice.setVisible(false);
        modifyProduct.lbl_oldDiscount.setVisible(false);

    }

    /**
     * Metodo para visualizar los componentes del panel antiguas propiedades
     */
    private void setVisibleOldPropertyes() {
        modifyProduct.lbl_oldPropertyes.setVisible(true);
        modifyProduct.jLabel1.setVisible(true);
        modifyProduct.jLabel2.setVisible(true);
        modifyProduct.jLabel3.setVisible(true);
        modifyProduct.jLabel4.setVisible(true);
        modifyProduct.jLabel5.setVisible(true);
        modifyProduct.lbl_oldReference.setVisible(true);
        modifyProduct.lbl_oldName.setVisible(true);
        modifyProduct.lbl_oldDescription.setVisible(true);
        modifyProduct.lbl_oldPrice.setVisible(true);
        modifyProduct.lbl_oldDiscount.setVisible(true);
    }

    /**
     * Metodo para actualizar las propiedades antiguas del panel
     *
     * @param data -- Datos a actualizar
     */
    public void updatePropertyesModify(ProductVO data) {

        setVisibleOldPropertyes();

        modifyProduct.lbl_oldReference.setText(data.getReference());
        modifyProduct.lbl_oldName.setText(data.getName());
        modifyProduct.lbl_oldDescription.setText(data.getDescription());
        modifyProduct.lbl_oldPrice.setText(data.getPrice() + " €");
        modifyProduct.lbl_oldDiscount.setText(data.getDiscount() + " €");

    }

    /**
     * Metodo para actualizar los campos del panel modificar productos
     *
     * @param data -- Datos para actualizar
     */
    private void updateFields(ProductVO data) {

        modifyProduct.txt_reference.setText(data.getReference());
        modifyProduct.txt_reference.setEnabled(false);

        modifyProduct.txt_name.setText(data.getName());
        modifyProduct.txta_description.setText(data.getDescription());
        modifyProduct.txt_price.setText(data.getPrice() + "");
        modifyProduct.txt_discount.setText(data.getDiscount() + "");

    }

    /**
     * Metodo que se ejcutara al pulsar el boton de insertar producto
     */
    private void insertProduct() {

        ProductVO producInserted = obtainProductDataInsert();

        logic.validateRegistryProduct(producInserted);

    }

    /**
     * Metodo que se ejecutar al pulsar boton de actualizar producto
     */
    private void updateProduct() {
        ProductVO producUpdate = obtainProductDataModify();

        logic.validateModifyProduct(producUpdate);
    }

    /**
     * Metodo que obtiene la informacion de los campos del dialogo de insertar
     * producto
     *
     * @return Objeto ProductVO con los datos
     */
    private ProductVO obtainProductDataInsert() {

        ProductVO product = new ProductVO();

        product.setReference(insertProducts.txt_reference.getText());
        product.setName(insertProducts.txt_name.getText());
        product.setDescription(insertProducts.txta_description.getText());

        try {

            product.setPrice(new BigDecimal(insertProducts.txt_price.getText()));
            product.setDiscount(new BigDecimal(insertProducts.txt_discount.getText()));

        } catch (NumberFormatException nfe) {

        }

        return product;
    }

    /**
     * Metodo para obtener la informacion de los campos del dialogo modificar
     * producto
     *
     * @return Objeto ProductVO con los datos
     */
    private ProductVO obtainProductDataModify() {

        ProductVO product = new ProductVO();

        product.setReference(modifyProduct.txt_reference.getText());
        product.setName(modifyProduct.txt_name.getText());
        product.setDescription(modifyProduct.txta_description.getText());

        try {

            if (!modifyProduct.txt_price.getText().isEmpty()) {
                product.setPrice(new BigDecimal(modifyProduct.txt_price.getText()));
            }

        } catch (NumberFormatException nfe) {
            modifyProduct.lbl_errorPrice.setText("Format");
            modifyProduct.lbl_errorPrice.setVisible(true);
        }

        try {
            if (!modifyProduct.txt_discount.getText().isEmpty()) {
                product.setDiscount(new BigDecimal(modifyProduct.txt_discount.getText()));
            }

        } catch (NumberFormatException nfe) {
            modifyProduct.lbl_errorDiscount.setText("Format");
            modifyProduct.lbl_errorDiscount.setVisible(true);
        }

        return product;
    }

    /**
     * Metodo para declarar un coordinador a la clase
     *
     * @param c
     */
    public void setCoordinator(Coordinator c) {

        this.coordinator = c;
        initComponents();

    }

    /**
     * Metodo para poner en la tabla todos los productos
     */
    public void setAllProducts() {

        Table_View_Product tableView = new Table_View_Product();

        JTable table = searchProducts.tableProducts;
        Object[] header = logic.getColumnsProduct();
        ArrayList<ProductVO> data = logic.validateQueryProducts();

        if (data.size() <= 0) {

            searchProducts.pnlTable.setVisible(false);
            searchProducts.noResults.setVisible(true);

        } else {

            searchProducts.pnlTable.setVisible(true);
            searchProducts.noResults.setVisible(false);

            tableView.setDataInTable(table, data, header);
            coordinator.getSearchProduct().tableProducts.updateUI();
        }

    }

    /**
     * Metodo que se ejcuta al perder el focu del campo referencia
     */
    private void actionFocusLostReference() {

        String reference = modifyProduct.txt_reference.getText();

        ArrayList<ProductVO> product = logic.validateQueryProductsByReference(reference);

        if (product.size() == 1) {
            modifyProduct.lbl_errorReference.setVisible(false);
            updatePropertyesModify(product.get(0));
            updateFields(product.get(0));
            modifyProduct.txt_reference.setEnabled(true);

        } else {

            setInvisibleOldPropertyes();
            modifyProduct.reset();

            modifyProduct.lbl_errorReference.setText("No product");
            modifyProduct.lbl_errorReference.setVisible(true);

        }

    }

    /**
     * Metodo que pone en la tabla los datos de los productosd en relacion a la
     * busqueda
     */
    public void setSearchProducts() {

        String search = getStringSearch();

        Table_View_Product tableView = new Table_View_Product();

        JTable table = searchProducts.tableProducts;
        Object[] header = logic.getColumnsProduct();
        ArrayList<ProductVO> data = logic.validateQueryProductsByReference(search);

        if (data.size() <= 0) {

            searchProducts.pnlTable.setVisible(false);

            searchProducts.noResults.setVisible(true);

        } else {

            searchProducts.pnlTable.setVisible(true);

            searchProducts.noResults.setVisible(false);

            tableView.setDataInTable(table, data, header);
            coordinator.getSearchProduct().tableProducts.updateUI();
        }

    }

    /**
     * MEtodo que obtiene los datos de la busqueda
     *
     * @return Cadena con la busqueda
     */
    private String getStringSearch() {

        return searchProducts.txt_search.getText();

    }

    /**
     * Metodo que se ejecuta al pulsar en el boton de Busqueda
     */
    private void pressedSearchProduct() {

        searchProducts.setLblColor(searchProducts.btn_search);
        searchProducts.resetLblColor(searchProducts.btn_all);

    }

    /**
     * Metodo que se ejecuta al pulsar en el boton de eliminar
     *
     * @param referenceProduct --> Referencia del producto que se va a eliminar
     */
    private void actionButtonDelete(String referenceProduct) {

        ProductVO productDelete = new ProductVO(referenceProduct);

        logic.validateDeleteProduct(productDelete);

    }

    /**
     * Metodo que gestiona los eventos de la tabla
     *
     * @param evt Evento que gestiona
     */
    private void eventTable(MouseEvent evt) {
        int clic_tabla = searchProducts.tableProducts.rowAtPoint(evt.getPoint());

        int column = searchProducts.tableProducts.getColumnModel().getColumnIndexAtX(evt.getX());
        int row = evt.getY() / searchProducts.tableProducts.getRowHeight();

        if (row < searchProducts.tableProducts.getRowCount() && row >= 0 && column < searchProducts.tableProducts.getColumnCount() && column >= 0) {
            Object value = searchProducts.tableProducts.getValueAt(row, column);
            if (value instanceof JButton) {
                ((JButton) value).doClick();
                JButton boton = (JButton) value;

                if (boton.getName().equals("m")) {

                    int rowSelected = searchProducts.tableProducts.getSelectedRow();

                    ProductVO productSelected = new ProductVO();

                    productSelected.setReference((String) searchProducts.tableProducts.getValueAt(rowSelected, 0));
                    productSelected.setName((String) searchProducts.tableProducts.getValueAt(rowSelected, 1));
                    productSelected.setDescription((String) searchProducts.tableProducts.getValueAt(rowSelected, 2));
                    productSelected.setPrice((BigDecimal) searchProducts.tableProducts.getValueAt(rowSelected, 3));
                    productSelected.setDiscount((BigDecimal) searchProducts.tableProducts.getValueAt(rowSelected, 4));

                    initViewModifyProduct(productSelected);

                }
                if (boton.getName().equals("e")) {

                    int rowSelected = searchProducts.tableProducts.getSelectedRow();

                    String referenceSelected = (String) searchProducts.tableProducts.getValueAt(rowSelected, 0);

                    actionButtonDelete(referenceSelected);

                }
            }

        }
    }

    /**
     * Clase interna para gestionar eventos de ventana
     */
    class EventsWindow extends WindowAdapter {

        @Override
        public void windowOpened(WindowEvent e) {

            if (e.getWindow() == searchProducts) {
                setAllProducts();
            }

        }

    }

    /**
     * Clase Interna para gestiona eventos del raton
     */
    class EventsMouse extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {

            if (searchProducts != null && searchProducts.isActive()) {

                if (e.getSource() == searchProducts.btn_all) {

                    coordinator.getSearchProduct().pulsedButton = 0;
                    setAllProducts();
                }

                if (e.getSource() == searchProducts.btn_search) {

                    coordinator.getSearchProduct().pulsedButton = 1;
                    setSearchProducts();
                }

                if (e.getSource() == searchProducts.tableProducts) {

                    eventTable(e);

                }
            }

            if (insertProducts != null && insertProducts.isActive()) {
                if (e.getSource() == insertProducts.btn_insert) {

                    insertProduct();

                }
            }

            if (modifyProduct != null && modifyProduct.isActive()) {
                if (e.getSource() == modifyProduct.btn_update) {

                    updateProduct();

                    if (!modifyProduct.txt_reference.isEnabled()) {
                        modifyProduct.dispose();
                    }

                }

            }

        }

    }

    /**
     * Clase interna para gestionar eventos de foco
     */
    class EventsFocus implements FocusListener {

        @Override
        public void focusGained(FocusEvent e) {

            if (searchProducts != null) {
                if (e.getSource() == searchProducts.txt_search) {

                    if (searchProducts.txt_search.getText().equals("Search...")) {

                        searchProducts.txt_search.setText("");

                    }

                }
                if (modifyProduct != null) {
                    if (e.getSource() == modifyProduct) {
                        modifyProduct.txt_reference.addFocusListener(eFocus);

                    }
                }

            }

        }

        @Override
        public void focusLost(FocusEvent e) {

            if (searchProducts != null) {
                if (e.getSource() == searchProducts.txt_search) {

                    if (searchProducts.txt_search.getText().equals("")) {

                        searchProducts.txt_search.setText("Search...");

                    }

                }
            }

            if (modifyProduct != null) {

                if (e.getSource() == modifyProduct.txt_reference) {

                    actionFocusLostReference();

                }

            }
        }

    }

    /**
     * Clase interna para gestionar eventos de teclado
     */
    class EventsKey extends KeyAdapter {

        @Override
        public void keyTyped(KeyEvent e) {
            System.out.println("-----------");
        }

        @Override
        public void keyPressed(KeyEvent e) {

            if (searchProducts != null) {

                int codeKeyPressed = e.getKeyCode();

                if (codeKeyPressed == KeyEvent.VK_ENTER) {
                    
                    pressedSearchProduct();
                    setSearchProducts();

                    coordinator.getSearchProduct().pulsedButton = 1;

                }
            }

        }

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.table;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

/**
 *
 * @author JStalin
 */
public class TableEditor extends AbstractCellEditor implements TableCellEditor, ActionListener{

    Boolean currentValue;
    JPanel panel;

    JButton modify;
    JButton delete;

    protected static final String EDIT = "edit";
    private JTable jTable1;

    public TableEditor(JTable jTable1) {
        panel = new JPanel();

        modify = new JButton("M");
        delete = new JButton("D");

        panel.add(modify);
        panel.add(delete);

        modify.setActionCommand(EDIT);
        modify.addActionListener(this);
        modify.setBorderPainted(false);
        modify.addMouseListener(new EventMouse());
        
        delete.setActionCommand(EDIT);
        delete.addActionListener(this);
        delete.setBorderPainted(false);
        delete.addMouseListener(new EventMouse());
        
        this.jTable1 = jTable1;
    }

    public void actionPerformed(ActionEvent e) {
        // mymodel t = (mymodel) jTable1.getModel();
        // t.addNewRecord();
        fireEditingStopped();
    }

    //Implement the one CellEditor method that AbstractCellEditor doesn't.
    public Object getCellEditorValue() {
        return currentValue;
    }

    //Implement the one method defined by TableCellEditor.
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        // Va a mostrar el botón solo en la última fila.
        // de otra forma muestra un espacio en blanco.
        //if (row == table.getModel().getRowCount() - 1) {
        currentValue = (Boolean) value;
        return panel;
        //}
        //return new JLabel();
    }
    
    
    class EventMouse extends MouseAdapter{

        @Override
        public void mouseClicked(MouseEvent e) {
            
            System.out.println("DSdsa");
            
            if(e.getSource() == modify){
                System.out.println("MODIFY");
            }
            
             if(e.getSource() == delete){
                 System.out.println("DELETE");
            }
            
        }
        
        
        
    }

}

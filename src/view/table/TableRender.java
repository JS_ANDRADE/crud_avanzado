/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author JStalin
 */
public class TableRender extends JLabel implements TableCellRenderer {

    boolean isBordered = true;

    public TableRender(boolean isBordered) {
        this.isBordered = isBordered;
        setOpaque(true);
    }

    public Component getTableCellRendererComponent(JTable table, Object color, boolean isSelected, boolean hasFocus, int row, int column) {
        // Va a mostrar el botón solo en la última fila.
        // de otra forma muestra un espacio en blanco.
        //if (row == table.getModel().getRowCount() - 1) {

        JPanel panel = new JPanel();

        JButton modify = new JButton("M");
        JButton delete = new JButton("D");

        panel.add(modify);
        panel.add(delete);

        class EventMouse extends MouseAdapter {

            @Override
            public void mouseClicked(MouseEvent e) {

                System.out.println("sdsa");
                
                if (e.getSource() == "modify") {
                    System.out.println("MODIFY");
                }

                if (e.getSource() == "delete") {
                    System.out.println("DELETE");
                }

            }

        }
        return panel;
        //} else {
        // setBackground(new Color(0xffffff));
        //return this;
        //}
    }

}

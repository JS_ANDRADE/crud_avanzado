/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import controller.*;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Logic;
import view.viewInsertProduct;
import view.viewSearchProducts;

import view.WindowMain;
import view.viewModifyProduct;

/**
 *
 * @author Marko
 */
public class Main {

    private static Coordinator coordinator;

    private static ControlProduct ctrlProduct;
    private static ControlVehicle ctrlVehicle;

    private static Logic logic;

    private static WindowMain wMain;

    public static void main(String[] args) {

        init();

    }

    private static void init() {

        
            coordinator = new Coordinator();

            ctrlProduct = new ControlProduct();
            ctrlVehicle = new ControlVehicle();

            logic = new Logic();

            wMain = new WindowMain();

            coordinator.setControlProduct(ctrlProduct);
            coordinator.setControlVehicle(ctrlVehicle);
            coordinator.setLogic(logic);
            coordinator.setWindowMain(wMain);

            ctrlProduct.setCoordinator(coordinator);
            ctrlVehicle.setCoordinator(coordinator);
            logic.setCoordinator(coordinator);
            wMain.setCoordinator(coordinator);

          

            wMain.setVisible(true);
       

    }

}

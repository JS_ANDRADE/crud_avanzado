
package model.vo;

import java.sql.Date;

/**
 * Clase para gestionar los datos de los vehiculos
 * @author JStalin
 */
public class VehicleVO {
    
    private String enrollment;
    private String brand;
    private String model;
    private int kilometers;
    private Date dateRevision;

    public VehicleVO() {
    }

    public VehicleVO(String enrollment) {
        this.enrollment = enrollment;
    }

    
    
    public VehicleVO(String enrollment, String brand, String model, int kilometers, Date dateRevision) {
        this.enrollment = enrollment;
        this.brand = brand;
        this.model = model;
        this.kilometers = kilometers;
        this.dateRevision = dateRevision;
    }

    public String getEnrollment() {
        return enrollment;
    }

    public void setEnrollment(String enrollment) {
        this.enrollment = enrollment;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getKilometers() {
        return kilometers;
    }

    public void setKilometers(int kilometers) {
        this.kilometers = kilometers;
    }

    public Date getDateRevision() {
        return dateRevision;
    }

    public void setDateRevision(Date dateRevision) {
        this.dateRevision = dateRevision;
    }
    
    
    
    
}

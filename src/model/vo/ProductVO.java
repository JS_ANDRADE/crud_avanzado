package model.vo;

import java.math.BigDecimal;
/**
 * Clase para gestionar los datos de los productos
 * @author JStalin
 */
public class ProductVO {

    private String reference;
    private String name;
    private String description;
    private BigDecimal price;
    private BigDecimal discount;

    public ProductVO() {
    }

    public ProductVO(String reference) {
        this.reference = reference;
    }

    public ProductVO(String reference, String name, String description, BigDecimal price, BigDecimal discount) {
        this.reference = reference;
        this.name = name;
        this.description = description;
        this.price = price;
        this.discount = discount;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

}

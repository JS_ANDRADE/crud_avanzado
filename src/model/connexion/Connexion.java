package model.connexion;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * Clase que para conexion a BD
 * @author JStalin
 */
public class Connexion {

    public static final String nameDatabase = "empresa";
    private static final String user = "root";
    private static final String password = "";
    private static final  String url = "jdbc:mysql://localhost:3306/" + nameDatabase;
    private static Connection connection = null;

    /**
     * Metodo que obtienen una conexion con la BD
     * @return Conexion obtenida
     */
    public static Connection getConnexion() {

        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = (Connection) DriverManager.getConnection(url, user, password);

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error in conexion with DataBase");

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, ex);

        }
        return connection;
    }

}

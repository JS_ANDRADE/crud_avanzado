/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Coordinator;
import java.math.BigDecimal;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.connexion.Connexion;
import model.dao.ProductDAO;
import model.dao.VehicleDAO;
import model.vo.ProductVO;
import model.vo.VehicleVO;
import view.viewInsertProduct;
import view.viewInsertVehicle;
import view.viewModifyProduct;
import view.viewModifyVehicle;

/**
 *Clase que gestiona todo la logica de la empresa y realiza las validaciones de 
 * datos
 * 
 * @author JStalin
 */
public class Logic {
    
    private Coordinator coordinator;
    
    /**
     * Metodo que valida que un registro de producto
     * @param product  Producto que se va a registrar
     */
    public void validateRegistryProduct(ProductVO product) {
        
        ProductDAO productDao = new ProductDAO();
        
        if (validateDataProductInsert(coordinator.getInserProduct())) {
            
            if (productDao.registerProduct(product)) {
                JOptionPane.showMessageDialog(coordinator.getInserProduct(), "Product Registred");
                coordinator.getInserProduct().reset();
            } else {
                JOptionPane.showMessageDialog(coordinator.getInserProduct(), "Could not product registred");
            }
            
        }
    }
    
    /**
     * Metodo que valida una modificacion de producto
     * @param product Producto que se valida
     */
    public void validateModifyProduct(ProductVO product) {
        
        ProductDAO productDao = new ProductDAO();
        
        if (validateDataProductModify(coordinator.getModifyProduct())) {
            
            if (productDao.modifyProduct(product)) {
                JOptionPane.showMessageDialog(coordinator.getModifyProduct(), "Product Modifycated");
                
                if (coordinator.getSearchProduct() != null) {
                    
                    if (coordinator.getSearchProduct().pulsedButton == 0) {
                        
                        coordinator.getControlProduct().setAllProducts();
                        
                    } else if (coordinator.getSearchProduct().pulsedButton == 1) {
                        
                        coordinator.getControlProduct().setSearchProducts();
                        
                    }
                }
                
                coordinator.getControlProduct().updatePropertyesModify(product);
                
            } else {
                JOptionPane.showMessageDialog(coordinator.getModifyProduct(), "Could not product modifyed");
            }
            
        }
        
    }
    
    /**
     * Metodo que valida una busqueda de productos
     * @return  Devuelve los productos que se buscan
     */
    public ArrayList<ProductVO> validateQueryProducts() {
        
        ProductDAO productDao = new ProductDAO();
        
        ArrayList<ProductVO> products = productDao.searchProducts();
        
        return products;
        
    }
    
    /**
     * Metodo que valida una busqueda de productos por referencia
     * @param searchProduct Devuelve la busqueda de productos
     * @return 
     */
    public ArrayList<ProductVO> validateQueryProductsByReference(String searchProduct) {
        ProductDAO productDao = new ProductDAO();
        
        ArrayList<ProductVO> products = productDao.searchProductByReference(searchProduct);
        
        return products;
    }
    
   
    /**
     * Metodo que valida la eliminacion de un producto
     * @param product Prodcto a eliminar
     */
    public void validateDeleteProduct(ProductVO product) {
        
        ProductDAO dao = new ProductDAO();
        
        int option = JOptionPane.showConfirmDialog(coordinator.getSearchProduct(), "¿Are you sure delete " + product.getReference() + " ?", "Confirm", JOptionPane.YES_NO_OPTION);
        
        if (option == JOptionPane.YES_OPTION) {
            
            if (dao.deleteProduct(product)) {
                
                JOptionPane.showMessageDialog(coordinator.getSearchProduct(), "Product Deleted Suscesfull");
                
                if (coordinator.getSearchProduct().pulsedButton == 0) {
                    
                    coordinator.getControlProduct().setAllProducts();
                    
                } else if (coordinator.getSearchProduct().pulsedButton == 1) {
                    
                    coordinator.getControlProduct().setSearchProducts();
                    
                }
                
            } else {
                JOptionPane.showMessageDialog(coordinator.getSearchProduct(), "Coul not be deleted product");
            }
        }
        
    }
    
    /**
     * Metodo que valida los datos de un producto para insertar
     * @param inserProduct Datos 
     * @return  True si es validdo, False si no lo es
     */
    private boolean validateDataProductInsert(viewInsertProduct inserProduct) {
        
        boolean b = true;
        
        if (inserProduct.txt_reference.getText().isEmpty()) {
            inserProduct.lbl_errorReference.setVisible(true);
            b = false;
        } else {
            inserProduct.lbl_errorReference.setVisible(false);
            
        }
        
        if (inserProduct.txt_name.getText().isEmpty()) {
            inserProduct.lbl_errorName.setVisible(true);
            b = false;
        } else {
            inserProduct.lbl_errorName.setVisible(false);
            
        }
        
        if (inserProduct.txt_price.getText().isEmpty()) {
            inserProduct.lbl_errorPrice.setText("Required");
            inserProduct.lbl_errorPrice.setVisible(true);
            
            b = false;
        } else {
            inserProduct.lbl_errorPrice.setVisible(false);
            
            try {
                
                new BigDecimal(inserProduct.txt_price.getText());
                
                inserProduct.lbl_errorPrice.setVisible(false);
            } catch (NumberFormatException e) {
                
                inserProduct.lbl_errorPrice.setText("Format");
                inserProduct.lbl_errorPrice.setVisible(true);
                b = false;
            }
            
        }
        
        if (inserProduct.txt_discount.getText().isEmpty()) {
            inserProduct.lbl_errorDiscount.setText("Required");
            inserProduct.lbl_errorDiscount.setVisible(true);
            
            b = false;
        } else {
            inserProduct.lbl_errorDiscount.setVisible(false);
            
            try {
                
                new BigDecimal(inserProduct.txt_discount.getText());
                
                inserProduct.lbl_errorDiscount.setVisible(false);
            } catch (NumberFormatException e) {
                
                inserProduct.lbl_errorDiscount.setText("Format");
                inserProduct.lbl_errorDiscount.setVisible(true);
                b = false;
            }
            
        }
        
        return b;
    }
    
    /**
     * Metodo que valida los datos de un producto a modificar
     * @param modifyProduct Datos
     * @return  True si es valido, False si no lo es
     */
    private boolean validateDataProductModify(viewModifyProduct modifyProduct) {
        
        boolean b = true;
        
        if (modifyProduct.txt_reference.getText().isEmpty()) {
            modifyProduct.lbl_errorReference.setVisible(true);
            b = false;
        } else {
            
            modifyProduct.lbl_errorReference.setVisible(false);
            
            if (modifyProduct.txt_name.getText().isEmpty()) {
                modifyProduct.lbl_errorName.setVisible(true);
                b = false;
            } else {
                modifyProduct.lbl_errorName.setVisible(false);
                
            }
            
            if (modifyProduct.txt_price.getText().isEmpty()) {
                modifyProduct.lbl_errorPrice.setVisible(true);
                b = false;
            } else {
                modifyProduct.lbl_errorPrice.setVisible(false);
                
                try {
                    new BigDecimal(modifyProduct.txt_price.getText());
                    modifyProduct.lbl_errorPrice.setVisible(false);
                } catch (NumberFormatException e) {
                    modifyProduct.lbl_errorPrice.setText("Format");
                    modifyProduct.lbl_errorPrice.setVisible(true);
                    b = false;
                }
            }
            
            if (modifyProduct.txt_discount.getText().isEmpty()) {
                modifyProduct.lbl_errorDiscount.setVisible(true);
                b = false;
            } else {
                modifyProduct.lbl_errorDiscount.setVisible(false);
                
                try {
                    new BigDecimal(modifyProduct.txt_discount.getText());
                    modifyProduct.lbl_errorDiscount.setVisible(false);
                } catch (NumberFormatException e) {
                    modifyProduct.lbl_errorDiscount.setText("Format");
                    modifyProduct.lbl_errorDiscount.setVisible(true);
                    b = false;
                }
            }
        }
        
        return b;
    }
    
    /**
     * Metodo que obtiene ek nombre de las columnas de la tabla productos
     * @return  Array con los nombres
     */
    public Object[] getColumnsProduct() {
        
        Object[] columns = new Object[0];
        ArrayList aux = new ArrayList();
        
        try {
            DatabaseMetaData metaData = Connexion.getConnexion().getMetaData();
            ResultSet columnsTable = metaData.getColumns(Connexion.nameDatabase, null, "Productos", null);
            
            while (columnsTable.next()) {
                
                aux.add(columnsTable.getString("COLUMN_NAME"));
                
            }
            
            columns = new Object[aux.size()];
            
            columns = aux.toArray(columns);
            
        } catch (SQLException ex) {
            Logger.getLogger(Logic.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return columns;
        
    }

    /**
     * **************************************************************************
     */
    
    /**
     * Metodo que valida el registro de un vehiculo
     * @param vehicle  Vehiculo a registrar
     */
    public void validateRegistryVehicle(VehicleVO vehicle) {
        
        VehicleDAO dao = new VehicleDAO();
        
        if (validateDataVehicleInsert(coordinator.getInsertVehicle())) {
            if (dao.registerVehicle(vehicle)) {
                JOptionPane.showMessageDialog(coordinator.getInsertVehicle(), "Vehicle Registred");
                coordinator.getInsertVehicle().reset();
            } else {
                JOptionPane.showMessageDialog(coordinator.getSearchProduct(), "Coul not be vehicle registred");
            }
        }
        
    }
    
    /**
     * Metodo que valida la modificacion de un vehiculo
     * @param vehicle  Vehiculo a validar
     */
    public void validateModifyVehicle(VehicleVO vehicle) {
        
        VehicleDAO dao = new VehicleDAO();
        
        if (validateDataVehicleModify(coordinator.getModifyVehicle())) {
            
            if (dao.modifyVehicle(vehicle)) {
                
                
                
                JOptionPane.showMessageDialog(coordinator.getModifyVehicle(), "Vehicle Modifycated");
                
                if (coordinator.getSearchVehicle() != null) {
                    
                    if (coordinator.getSearchVehicle().pulsedButton == 0) {
                        
                        coordinator.getControlVehicle().setAllVehicles();
                        
                    } else if (coordinator.getSearchVehicle().pulsedButton == 1) {
                        
                        coordinator.getControlVehicle().setSearchVehicles();
                        
                    }
                }
                
                coordinator.getControlVehicle().updatePropertyesModify(vehicle);
            } else {
                JOptionPane.showMessageDialog(coordinator.getSearchProduct(), "Coul not be vehicle modified");
            }
            
        }
        
    }
    
    /**
     * Metodo que valida la bsuqueda de un vehiculo
     * @return  Array con la busqueda
     */
    public ArrayList<VehicleVO> validateQueryVehicles() {
        
        VehicleDAO vehicleDao = new VehicleDAO();
        
        ArrayList<VehicleVO> vehicles = vehicleDao.searchVehicles();
        
        return vehicles;
        
    }
    
    /**
     * Metood que valida la busqueda de un vehiculo segun la matricula
     * @param searchVehicle Vehiculo de busqueda
     * @return Vehiculos encontrados
     */
    public ArrayList<VehicleVO> validateQueryVehiclesBySearch(String searchVehicle) {
        VehicleDAO vehicleDao = new VehicleDAO();
        
        ArrayList<VehicleVO> vehicles = vehicleDao.searchVehiclesBySearch(searchVehicle);
        
        System.out.println(vehicles.size());
        
        return vehicles;
    }
    
    public void validateDeleteVehicle(VehicleVO vehicle) {
        
        VehicleDAO dao = new VehicleDAO();
        
        int option = JOptionPane.showConfirmDialog(coordinator.getSearchVehicle(), "¿Are you sure delete " + vehicle.getEnrollment() + " ?", "Confirm", JOptionPane.YES_NO_OPTION);
        
        if (option == JOptionPane.YES_OPTION) {
            
            if (dao.deleteVehicle(vehicle)) {
                
                JOptionPane.showMessageDialog(coordinator.getSearchVehicle(), "Vehicle Deleted Suscesfull");
                
                if (coordinator.getSearchVehicle().pulsedButton == 0) {
                    
                    coordinator.getControlVehicle().setAllVehicles();
                    
                } else if (coordinator.getSearchVehicle().pulsedButton == 1) {
                    
                    coordinator.getControlVehicle().setSearchVehicles();
                    
                }
                
            } else {
                JOptionPane.showMessageDialog(coordinator.getSearchVehicle(), "Coul not be deleted vehicle");
            }
        }
        
    }
    
    /**
     * Metodo que valida los datos de un vehiculo para insertar
     * @param inserVehicle 
     * @return True si es valido, False si no lo es
     */
    private boolean validateDataVehicleInsert(viewInsertVehicle inserVehicle) {
        
        boolean b = true;
        
        if (inserVehicle.txt_enrollment.getText().isEmpty()) {
            inserVehicle.lbl_errorEnrollment.setVisible(true);
            b = false;
        } else {
            inserVehicle.lbl_errorEnrollment.setVisible(false);
            
        }
        
        if (inserVehicle.txt_brand.getText().isEmpty()) {
            inserVehicle.lbl_errorBrand.setVisible(true);
            b = false;
        } else {
            inserVehicle.lbl_errorBrand.setVisible(false);
            
        }
        
        if (inserVehicle.txt_kilometers.getText().isEmpty()) {
            inserVehicle.lbl_errorKilometers.setVisible(true);
            b = false;
        } else {
            inserVehicle.lbl_errorKilometers.setVisible(false);
            
            try {
                Integer.parseInt(inserVehicle.txt_kilometers.getText());
                inserVehicle.lbl_errorKilometers.setVisible(false);
            } catch (NumberFormatException nfe) {
                
                inserVehicle.lbl_errorKilometers.setText("Format");
                inserVehicle.lbl_errorKilometers.setVisible(true);
                
                b = false;
            }
        }
        
        if (inserVehicle.txt_dateReview.getText().isEmpty()) {
            inserVehicle.lbl_errorDateReview.setVisible(true);
            b = false;
        } else {
            inserVehicle.lbl_errorDateReview.setVisible(false);
            
            try {
                
                SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy");
                
                formatDate.parse(inserVehicle.txt_dateReview.getText());
                
                inserVehicle.lbl_errorDateReview.setVisible(false);
            } catch (ParseException pe) {
                
                inserVehicle.lbl_errorDateReview.setText("Format");
                inserVehicle.lbl_errorDateReview.setVisible(true);
                
                b = false;
            }
        }
        
        return b;
    }
    
    /**
     * Metodo que valida los datos de modificacion de un vheiculo
     * @param modifyVehicle
     * @return True si es valido, Flase si no lo es
     */
    private boolean validateDataVehicleModify(viewModifyVehicle modifyVehicle) {
        
        boolean b = true;
        
        if (modifyVehicle.txt_enrollment.getText().isEmpty()) {
            modifyVehicle.lbl_errorEnrollment.setVisible(true);
            b = false;
        } else {
            
            if (modifyVehicle.txt_brand.getText().isEmpty()) {
                modifyVehicle.lbl_errorBrand.setVisible(true);
                b = false;
            } else {
                modifyVehicle.lbl_errorBrand.setVisible(false);
                
            }
            
            if (modifyVehicle.txt_kilometers.getText().isEmpty()) {
                modifyVehicle.lbl_errorKilometers.setVisible(true);
                b = false;
            } else {
                modifyVehicle.lbl_errorKilometers.setVisible(false);
                
                try {
                    Integer.parseInt(modifyVehicle.txt_kilometers.getText());
                    modifyVehicle.lbl_errorKilometers.setVisible(false);
                } catch (NumberFormatException nfe) {
                    
                    modifyVehicle.lbl_errorKilometers.setText("Format");
                    modifyVehicle.lbl_errorKilometers.setVisible(true);
                    
                    b = false;
                }
            }
            
            if (modifyVehicle.txt_dateReview.getText().isEmpty()) {
                modifyVehicle.lbl_errorDateReview.setVisible(true);
                b = false;
            } else {
                modifyVehicle.lbl_errorDateReview.setVisible(false);
                
                try {
                    
                    SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy");
                    
                    formatDate.parse(modifyVehicle.txt_dateReview.getText());
                    
                    modifyVehicle.lbl_errorDateReview.setVisible(false);
                } catch (ParseException pe) {
                    
                    modifyVehicle.lbl_errorDateReview.setText("Format");
                    modifyVehicle.lbl_errorDateReview.setVisible(true);
                    
                    b = false;
                }
            }
            
        }
        
        return b;
    }
    
    
    /**
     * Metodo que obtiene las columans de la tabla vehiculos
     * @return Array con el nombre de las columnas
     */
    public Object[] getColumnsVehicle() {
        
        Object[] columns = new Object[0];
        ArrayList aux = new ArrayList();
        
        try {
            DatabaseMetaData metaData = Connexion.getConnexion().getMetaData();
            ResultSet columnsTable = metaData.getColumns(Connexion.nameDatabase, null, "Vehiculos", null);
            
            while (columnsTable.next()) {
                
                aux.add(columnsTable.getString("COLUMN_NAME"));
                
            }
            
            columns = new Object[aux.size()];
            
            columns = aux.toArray(columns);
            
        } catch (SQLException ex) {
            Logger.getLogger(Logic.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return columns;
        
    }
    
    /**
     * Metodo para asignar un coordinador a la clase
     * @param c Coordinador a asignar
     */
    public void setCoordinator(Coordinator c) {
        
        this.coordinator = c;
        
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.connexion.Connexion;
import model.vo.VehicleVO;

/**
 * Clase para realizar operaciones sobre tabla Vehiculos
 *
 * @author JStalin
 */
public class VehicleDAO extends Connexion {

    private final String SQL_INSERT_VEHICLE = "INSERT INTO vehiculos (matricula, marca, modelo , nKilometros, fRevision) VALUES(?,?,?,?,?)";
    private final String SQL_UPDATE_VEHICLE = "UPDATE vehiculos SET matricula=?, marca=?, modelo=?, nKilometros=?, fRevision=? WHERE matricula=?";
    private final String SQL_DELETE_VEHICLE = "DELETE FROM vehiculos WHERE matricula=? ";
    private final String SQL_QUERY_VEHICLE = "SELECT * FROM vehiculos";
    private final String SQL_QUERY_VEHICLE_SEARCH = "SELECT * FROM vehiculos WHERE matricula=?";

    /**
     * Metodo apra registrar un vehiculo
     *
     * @param vehicle Vehiculoa registrar
     * @return Ttue si se ha registrado, False si no
     */
    public boolean registerVehicle(VehicleVO vehicle) {

        PreparedStatement sentenceInsert = null;
        Connection connexion = getConnexion();

        try {
            sentenceInsert = connexion.prepareStatement(SQL_INSERT_VEHICLE);

            sentenceInsert.setString(1, vehicle.getEnrollment());
            sentenceInsert.setString(2, vehicle.getBrand());
            sentenceInsert.setString(3, vehicle.getModel());
            sentenceInsert.setInt(4, vehicle.getKilometers());
            sentenceInsert.setDate(5, vehicle.getDateRevision());

            sentenceInsert.execute();

            return true;
        } catch (SQLException e) {
            System.err.println(e);
            return false;
        } finally {
            try {
                sentenceInsert.close();
                connexion.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

     /**
     * Metodo para modificar un vehiculo
     *
     * @param vehicle Vehiculo a modificar
     * @return True si se ha registrado, False si no
     */
    public boolean modifyVehicle(VehicleVO vehicle) {

        PreparedStatement sentenceUpdate = null;
        Connection connexion = getConnexion();

        try {
            sentenceUpdate = connexion.prepareStatement(SQL_UPDATE_VEHICLE);

            sentenceUpdate.setString(1, vehicle.getEnrollment());
            sentenceUpdate.setString(2, vehicle.getBrand());
            sentenceUpdate.setString(3, vehicle.getModel());
            sentenceUpdate.setInt(4, vehicle.getKilometers());
            sentenceUpdate.setDate(5, vehicle.getDateRevision());

            sentenceUpdate.setString(6, vehicle.getEnrollment());

            sentenceUpdate.execute();
            return true;
        } catch (SQLException e) {
            System.err.println(e);
            return false;
        } finally {
            try {

                sentenceUpdate.close();
                connexion.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

    /**
     * Metodo para eliminar un vehiculo
     *
     * @param vehicle Vehiculo a eliminar
     * @return True si se ha eliminado, False si no
     */
    public boolean deleteVehicle(VehicleVO vehicle) {
        PreparedStatement sentenceDelete = null;
        Connection connexion = getConnexion();

        try {
            sentenceDelete = connexion.prepareStatement(SQL_DELETE_VEHICLE);

            sentenceDelete.setString(1, vehicle.getEnrollment());

            sentenceDelete.execute();
            return true;
        } catch (SQLException e) {
            System.err.println(e);
            return false;
        } finally {
            try {
                sentenceDelete.close();
                connexion.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

    /**
     * Metodo para buscar todos los vehiculos
     * @return Vehiculos
     */
    public ArrayList<VehicleVO> searchVehicles() {

        ArrayList<VehicleVO> vehicles = new ArrayList<VehicleVO>();
        VehicleVO auxProduct = null;

        PreparedStatement sentenceQuery = null;
        ResultSet resultQuery = null;
        Connection connexion = getConnexion();

        try {

            sentenceQuery = connexion.prepareStatement(SQL_QUERY_VEHICLE);
            resultQuery = sentenceQuery.executeQuery();

            while (resultQuery.next()) {

                auxProduct = new VehicleVO();

                auxProduct.setEnrollment(resultQuery.getString("matricula"));
                auxProduct.setBrand(resultQuery.getString("marca"));
                auxProduct.setModel(resultQuery.getString("modelo"));
                auxProduct.setKilometers(resultQuery.getInt("nKilometros"));
                auxProduct.setDateRevision(resultQuery.getDate("fRevision"));

                vehicles.add(auxProduct);

            }

        } catch (SQLException e) {
            System.err.println(e);
            return null;
        } finally {
            try {
                sentenceQuery.close();
                connexion.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }

        return vehicles;

    }

    /**
     * Metodo para buscar vehciulos segun su matricula
     * @param searchProduct Vehiculo a buscar
     * @return Vehiculos
     */
    public ArrayList<VehicleVO> searchVehiclesBySearch(String searchProduct) {

        ArrayList<VehicleVO> vehicles = new ArrayList<VehicleVO>();
        VehicleVO auxProduct = null;

        PreparedStatement sentenceQuery = null;
        ResultSet resultQuery = null;
        Connection connexion = getConnexion();

        try {

            sentenceQuery = connexion.prepareStatement(SQL_QUERY_VEHICLE_SEARCH);

            if (searchProduct.equals("")) {
                searchProduct = "null";
            }

            sentenceQuery.setString(1, searchProduct);

            resultQuery = sentenceQuery.executeQuery();

            while (resultQuery.next()) {

                auxProduct = new VehicleVO();

                auxProduct.setEnrollment(resultQuery.getString("matricula"));
                auxProduct.setBrand(resultQuery.getString("marca"));
                auxProduct.setModel(resultQuery.getString("modelo"));
                auxProduct.setKilometers(resultQuery.getInt("nKilometros"));
                auxProduct.setDateRevision(resultQuery.getDate("fRevision"));

                vehicles.add(auxProduct);

            }

        } catch (SQLException e) {
            System.err.println(e);
            return null;
        } finally {
            try {
                sentenceQuery.close();
                connexion.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }

        return vehicles;
    }

}

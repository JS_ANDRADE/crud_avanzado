/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.connexion.Connexion;
import model.vo.ProductVO;

/**
 * Clase para realizar operaciones sobre la tabla productos
 *
 * @author JStalin
 */
public class ProductDAO extends Connexion {

    private final String SQL_INSERT_PRODUCT = "INSERT INTO productos (referencia, nombre, descripcion , precio, descuento) VALUES(?,?,?,?,?)";
    private final String SQL_UPDATE_PRODUCT = "UPDATE productos SET referencia=?, nombre=?, descripcion=?, precio=?, descuento=? WHERE referencia=?";
    private final String SQL_DELETE_PRODUCT = "DELETE FROM productos WHERE referencia=? ";
    private final String SQL_QUERY_PRODUCT = "SELECT * FROM productos ORDER BY nombre ";
    private final String SQL_QUERY_PRODUCT_SEARCH_REFERENCE = "SELECT * FROM productos WHERE referencia=? ORDER BY nombre";

    /**
     * Metodo apra registrar un producto
     *
     * @param product Producto a registrar
     * @return Ttue si se ha registrado, False si no
     */
    public boolean registerProduct(ProductVO product) {

        PreparedStatement sentenceInsert = null;
        Connection connexion = getConnexion();

        try {
            sentenceInsert = connexion.prepareStatement(SQL_INSERT_PRODUCT);

            sentenceInsert.setString(1, product.getReference());
            sentenceInsert.setString(2, product.getName());
            sentenceInsert.setString(3, product.getDescription());
            sentenceInsert.setBigDecimal(4, product.getPrice());
            sentenceInsert.setBigDecimal(5, product.getDiscount());

            sentenceInsert.execute();

            return true;
        } catch (SQLException e) {
            System.err.println(e);
            return false;
        } finally {
            try {
                sentenceInsert.close();
                connexion.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

    /**
     * Metodo para modificar un producto
     *
     * @param product Producto a modificar
     * @return Ttue si se ha modificado, False si no
     */
    public boolean modifyProduct(ProductVO product) {

        PreparedStatement sentenceUpdate = null;
        Connection connexion = getConnexion();

        try {
            sentenceUpdate = connexion.prepareStatement(SQL_UPDATE_PRODUCT);

            sentenceUpdate.setString(1, product.getReference());
            sentenceUpdate.setString(2, product.getName());
            sentenceUpdate.setString(3, product.getDescription());
            sentenceUpdate.setBigDecimal(4, product.getPrice());
            sentenceUpdate.setBigDecimal(5, product.getDiscount());

            sentenceUpdate.setString(6, product.getReference());

            sentenceUpdate.execute();
            return true;
        } catch (SQLException e) {
            System.err.println(e);
            return false;
        } finally {
            try {

                sentenceUpdate.close();
                connexion.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

    /**
     * Metodo para eliminar un producto
     *
     * @param product Producto a eliminar
     * @return Ttue si se ha eliminado, False si no
     */
    public boolean deleteProduct(ProductVO product) {
        PreparedStatement sentenceDelete = null;
        Connection connexion = getConnexion();

        try {
            sentenceDelete = connexion.prepareStatement(SQL_DELETE_PRODUCT);

            sentenceDelete.setString(1, product.getReference());

            sentenceDelete.execute();
            return true;
        } catch (SQLException e) {
            System.err.println(e);
            return false;
        } finally {
            try {
                sentenceDelete.close();
                connexion.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }

    /**
     * Metodo para realizar busqueda de todos los productos
     * @return Productos
     */
    public ArrayList<ProductVO> searchProducts() {

        ArrayList<ProductVO> products = new ArrayList<ProductVO>();
        ProductVO auxProduct = null;

        PreparedStatement sentenceQuery = null;
        ResultSet resultQuery = null;
        Connection connexion = getConnexion();

        try {

            sentenceQuery = connexion.prepareStatement(SQL_QUERY_PRODUCT);
            resultQuery = sentenceQuery.executeQuery();

            while (resultQuery.next()) {

                auxProduct = new ProductVO();

                auxProduct.setReference(resultQuery.getString("referencia"));
                auxProduct.setName(resultQuery.getString("nombre"));
                auxProduct.setDescription(resultQuery.getString("descripcion"));
                auxProduct.setPrice(resultQuery.getBigDecimal("precio"));
                auxProduct.setDiscount(resultQuery.getBigDecimal("descuento"));

                products.add(auxProduct);

            }

        } catch (SQLException e) {
            System.err.println(e);

        } finally {
            try {
                sentenceQuery.close();
                connexion.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }

        return products;

    }

    /**
     * Metodo para realizar busqueda de productos segun la referencia
     * @param reference Refernecia buscada
     * @return Productos
     */
    public ArrayList<ProductVO> searchProductByReference(String reference) {

        ArrayList<ProductVO> products = new ArrayList<ProductVO>();
        ProductVO auxProduct = null;

        PreparedStatement sentenceQuery = null;
        ResultSet resultQuery = null;
        Connection connexion = getConnexion();

        try {

            sentenceQuery = connexion.prepareStatement(SQL_QUERY_PRODUCT_SEARCH_REFERENCE);

            if (reference.equals("")) {
                reference = "null";
            }

            sentenceQuery.setString(1, reference);

            resultQuery = sentenceQuery.executeQuery();

            while (resultQuery.next()) {

                auxProduct = new ProductVO();

                auxProduct.setReference(resultQuery.getString("referencia"));
                auxProduct.setName(resultQuery.getString("nombre"));
                auxProduct.setDescription(resultQuery.getString("descripcion"));
                auxProduct.setPrice(resultQuery.getBigDecimal("precio"));
                auxProduct.setDiscount(resultQuery.getBigDecimal("descuento"));

                products.add(auxProduct);

            }

        } catch (SQLException e) {
            System.err.println(e);

        } finally {
            try {
                sentenceQuery.close();
                connexion.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }

        return products;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table;

import controller.Coordinator;
import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import model.dao.ProductDAO;
import model.vo.ProductVO;
import model.vo.VehicleVO;

/**
 *Clase para gestionar la tabla de los vehiculos
 * 
 * @author JStalin
 */
public class Table_View_Vehicle {

    /**
     * Metodo para mostrar datos en la tabla
     * @param table Tabla en la que se va  mostrar
     * @param data Datos a mostrar
     * @param header Cabecera de los datos
     */
    public void setDataInTable(JTable table, ArrayList<VehicleVO> data, Object[] header) {

        table.setDefaultRenderer(Object.class, new Render());
        DefaultTableModel dt = new DefaultTableModel() {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

       
        dt.addColumn("Enrollment");
        dt.addColumn("Brand");
        dt.addColumn("Model");
        dt.addColumn("Kilometers");
        dt.addColumn("Date of Revire");
        dt.addColumn("Modify");
        dt.addColumn("Delete");

        JButton btn_modify = new JButton("");
        ImageIcon iconModify = new ImageIcon(getClass().getResource("/view/icons/icon_modify_table.png"));
        //ImageIcon iconModificarRollever = new ImageIcon(getClass().getResource("/view/icons/icon_modify_table.png"));
        btn_modify.setIcon(iconModify);
        btn_modify.setContentAreaFilled(false);
        btn_modify.setBorder(null);
        btn_modify.setRolloverEnabled(true);
        //btn_modificar.setRolloverIcon(iconModificarRollever);
        //btn_modificar.setPressedIcon(iconModificarRollever);
        //btn_modificar.setRolloverSelectedIcon(iconModificarRollever);
        //btn_modificar.setMaximumSize(new Dimension(30,30));
        btn_modify.setName("m");

        JButton btn_delete = new JButton("");
        ImageIcon iconDelete = new ImageIcon(getClass().getResource("/view/icons/ícon_delete_table.png"));
        btn_delete.setIcon(iconDelete);
        btn_delete.setContentAreaFilled(false);
        btn_delete.setBorder(null);
        btn_delete.setRolloverEnabled(true);
        btn_delete.setName("e");

        ArrayList<VehicleVO> list = data;

       VehicleVO vehicle = new VehicleVO();

        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                Object fila[] = new Object[7];
                vehicle = list.get(i);
                fila[0] = vehicle.getEnrollment();
                fila[1] = vehicle.getBrand();
                fila[2] = vehicle.getModel();
                fila[3] = vehicle.getKilometers();
                fila[4] = vehicle.getDateRevision();
                fila[5] = btn_modify;
                fila[6] = btn_delete;
                dt.addRow(fila);
            }

            table.setModel(dt);

            table.getColumnModel().getColumn(5).setPreferredWidth(25);
            table.getColumnModel().getColumn(6).setPreferredWidth(25);

            table.setRowHeight(25);

        }
    }

}
